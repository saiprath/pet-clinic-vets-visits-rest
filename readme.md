This is a modified version of the original pet clinic app for adding vets 
when creating appointments for pets

## Running petclinic locally
```
	git clone https://gitlab.com/saiprath/pet-clinic-vets-visits-rest.git
	cd pet-clinic-vets-visits-rest
	./mvnw spring-boot:run
```
You can then access petclinic here: http://localhost:9966/petclinic/

For other information look into 
https://github.com/spring-petclinic/spring-petclinic-rest.git
