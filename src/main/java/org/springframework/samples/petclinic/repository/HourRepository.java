
package org.springframework.samples.petclinic.repository;

import java.util.Collection;

import org.springframework.dao.DataAccessException;
import org.springframework.samples.petclinic.model.Hour;

/**
 * @author Sai Gunturu
 *
 */

public interface HourRepository {
	
	Hour findById(int id) throws DataAccessException;
	
	Collection<Hour> findAll() throws DataAccessException;
	

}
