/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.springframework.samples.petclinic.rest;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.samples.petclinic.model.Hour;
import org.springframework.samples.petclinic.model.Pet;
import org.springframework.samples.petclinic.model.Vet;
import org.springframework.samples.petclinic.model.Visit;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

/**
 * @author Vitaliy Fedoriv
 *
 */

public class JacksonCustomVisitDeserializer extends StdDeserializer<Visit> {

	public JacksonCustomVisitDeserializer() {
		this(null);
	}

	public JacksonCustomVisitDeserializer(Class<Visit> t) {
		super(t);
	}

	@Override
	public Visit deserialize(JsonParser parser, DeserializationContext context)	throws IOException, JsonProcessingException {
		
		try {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
		Visit visit = new Visit();
		Pet pet = new Pet();
		//ObjectMapper mapper = new ObjectMapper();
		Date visitDate = null;
		JsonNode node = parser.getCodec().readTree(parser);
		JsonNode pet_node = node.get("pet");
		//pet = mapper.treeToValue(pet_node, Pet.class);
		int petId = pet_node.get("id").asInt();
		
		pet.setId(petId);
		//int visitId = node.get("id").asInt();
		String visitDateStr = node.get("date").asText(null);
		String description = node.get("description").asText(null);
		try {
			
			visitDate = formatter.parse(visitDateStr);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new IOException(e);
		}
		JsonNode vet_node = node.get("vet");
		Vet vet = new Vet();
		//Vet vet = mapper.treeToValue(vet_node, Vet.class);
		int vetId = vet_node.get("id").asInt();
		vet.setId(vetId);
		JsonNode hour_node = node.get("hour");
		Hour hour = new Hour();
		int hourId = hour_node.get("id").asInt();
		hour.setId(hourId);
		//Hour hour = mapper.treeToValue(hour_node, Hour.class);

		/*if (!(visitId == 0)) {
			visit.setId(visitId);
		}*/
		visit.setDate(visitDate);
		visit.setDescription(description);
		visit.setPet(pet);
		visit.setVet(vet);
		visit.setHour(hour);
		
		
		return visit;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}

}
